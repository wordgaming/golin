# Golin word game (by Codefish)

It is a simple word game that is directly inspired from the Lingo TV show.
The player must find words with several guesses; for each guess, the position of the well-placed and misplaced words are supplied.
One has a limited number of guesses to find the word.

This project is developed using the [React library](https://reactjs.org/).

It can be built with the command `npm run build`.
A local development server can be started with `npm run start`.
Do not forget to download the referenced dictionaries (used to pick and check words) with `npm run downloadDictionaries` before. They are placed in the `public/dictionaries` directory.

This project has not been thoroughly tested; it may contain bugs.
Report them to [me](mailto:codefish@online.fr).
This projet is released under [the Affero General Public License](./LICENSE).