/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { useMemo } from "react"
import { Link, useNavigate, useParams, useSearchParams } from "react-router-dom"
import { b64_to_utf8, utf8_to_b64 } from "../utils/stringutils"
import { Game, GameInputProps } from "./Game"

import defaultConfig from '../config/defaultConfig.json'
import dictionaries from '../config/dictionaries.json'

import { DictionaryManager, DictionaryName } from "../model/dictionary"

export interface GameWrapperState extends GameInputProps {
    dictionaryName: DictionaryName|undefined
}

function handleInt(value: string|null, defval: number): number {
    if (value === null) return defval
    let parsed = parseInt(value)
    if (isFinite(parsed)) return parsed
    return defval
}

function useGameWrapperState(): GameWrapperState {
    const params = useParams()
    const [searchParams] = useSearchParams()
    return useMemo<GameWrapperState>(() => {
        const uncheckedDictionaryName = searchParams.get('dictionaryName')
        const dictionaryName = (uncheckedDictionaryName && uncheckedDictionaryName in dictionaries) ? uncheckedDictionaryName as DictionaryName : undefined
        const answer = params.answer ? b64_to_utf8(params.answer) : ''
        const rowNumber = handleInt(searchParams.get('rowNumber'), defaultConfig.rowNumber)
        const gifts = searchParams.get('gifts')?.split('').map(x => x === '1') || Array(answer.length).fill(false)
        const propositions = searchParams.get('propositions')?.split('-').filter(x => x.length >= 2).map(x => { return {word: x.substring(1), isValid: x.substring(0, 1) === '1'} }) || []
        const answerShown = searchParams.get('answerShown') === '1'
        const invalidPropositionsShown = searchParams.get('invalidPropositionsShown') === '1'
        // retrieve asynchronoously the dictionary
        if (dictionaryName) DictionaryManager.INSTANCE.getDictionary(dictionaryName)
        return {rowNumber: rowNumber, dictionaryName: dictionaryName, answer: answer, gifts: gifts, propositions: propositions, answerShown: answerShown, invalidPropositionsShown: invalidPropositionsShown}
    }, [params, searchParams])
}

export function getGameWrapperStateURL(state: GameWrapperState) {
    const obfuscatedAnswer = utf8_to_b64(state.answer)
    const dictionaryName = state.dictionaryName ? encodeURIComponent(state.dictionaryName) : ''
    const searchParams = new URLSearchParams()
    searchParams.set('dictionaryName', `${dictionaryName}`)
    searchParams.set('rowNumber', `${state.rowNumber}`)
    searchParams.set('gifts', state.gifts.map(x => x ? '1' : '0').join(''))
    searchParams.set('propositions', state.propositions.map(x => `${x.isValid ? 1 : 0}${x.word}`).join('-'))
    searchParams.set('invalidPropositionsShown', `${state.invalidPropositionsShown ? 1 : 0}`)
    searchParams.set('answerShown', `${state.answerShown ? 1 : 0}`)
    return `/game/${obfuscatedAnswer}?${searchParams.toString()}`
}

// The game wrapper wraps a game for the router
export const GameWrapper = () => {
    const state = useGameWrapperState()
    const navigate = useNavigate()
    const onNewProposition = async (word: string) => {
        const dictionary = state.dictionaryName ? await DictionaryManager.INSTANCE.getDictionary(state.dictionaryName) : undefined
        const isValid = dictionary ? dictionary.checkWord(word) : true
        let newState = {...state, propositions: [...state.propositions, {word: word, isValid: isValid}]}
        navigate(getGameWrapperStateURL(newState), {replace: true})
    }
    const revealAnswer = () => {
        navigate(getGameWrapperStateURL({...state, answerShown: true}), {replace: true})
    }
    const answerFound = state.propositions.some(x => x.word === state.answer)
    return <div id="GameWrapper" style={{display: 'flex', flexDirection: 'column', gap: '10px', flex: 1}}>
        <Game {...state} onNewProposition={onNewProposition} />
        <div style={{display: 'flex', flexDirection: 'row', gap: "5px", justifyContent: 'center'}}>
            <div><Link to={getGameWrapperStateURL({...state, propositions: [], answerShown: false})}>🔗Empty grid</Link></div>
            {state.propositions.length > 0 && <div><Link to={getGameWrapperStateURL({...state, answerShown: false})}>🔗Filled grid</Link></div>}
            {!(state.answerShown || answerFound) && <div><button onClick={revealAnswer}>Reveal answer</button></div>}
        </div>
    </div>
}