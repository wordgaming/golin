/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { CSSProperties, useCallback, useEffect, useMemo, useState } from "react"
import { Grid, Proposition } from "./Grid"
import { Keyboard } from "./Keyboard"

import './Game.css'
import { computePlacement } from "../utils/placement"
import { LetterStatus } from "./Letter"

export const enum GameStatus { None, Playing, Victory, Defeat, AnswerShown }

export interface GameInputProps {
    rowNumber: number // number of rows in the grid
    answer: string // the word that must be found by the player
    gifts: boolean[] // letters that are given to the player (true value for a given letter)
    propositions: Proposition[] // to populate the rows with initial propositions
    invalidPropositionsShown: boolean // if the invalid propositions are shown
    answerShown: boolean // should the answer be shown? (typically this prop is modified if the player gives up)
}

export interface GameOutputProps {
    onNewProposition: (proposition: string) => void
}

export function getGameFeedback(props: GameInputProps) {
    function getPlacementStr(proposition: string) {
        let placement = computePlacement(proposition, props.answer)
        let result = []
        for (let i = 0; i < placement.length; i++)
            if (props.gifts[i])
                result.push(props.answer.charAt(i))
            else switch(placement[i]) {
                case LetterStatus.Absent: result.push('□'); break
                case LetterStatus.Misplaced: result.push('●'); break
                case LetterStatus.Placed: result.push('■'); break
            } 
        return result.join('')
    }

    let result = []
    let j = 0
    for (let i = 0; i < props.propositions.length; i++) {
        let proposition = props.propositions[i]
        let shown = props.invalidPropositionsShown || proposition.isValid
        if (shown) {
            if (!proposition.isValid)
                result.push(`#${++j}: invalid word`)
            else
                result.push(`#${++j}: ${getPlacementStr(proposition.word)}`)
        }
    }
    let legend = '■: placed; ●: misplaced; □: absent'
    return `${legend}\n${result.join('\n')}`
}

export const Game = (props: GameInputProps & GameOutputProps) => {
    const [editedProposition, setEditedProposition] = useState<string>("")

    const rawPropositions = useMemo<Proposition[]>(() => props.propositions.filter(x => props.invalidPropositionsShown || x.isValid || x.word === props.answer), 
        [props.propositions, props.answer])

    const status = useMemo<GameStatus>(() => {
        if (props.answerShown) return GameStatus.AnswerShown
        if (rawPropositions.some(x => x.word === props.answer)) return GameStatus.Victory
        if (rawPropositions.length >= props.rowNumber) return GameStatus.Defeat
        return GameStatus.Playing
    }, [rawPropositions, props.answer, props.answerShown, props.rowNumber])
    

    const propositions = useMemo<Proposition[]>(() => {
        let p = rawPropositions
        if (! props.answerShown)
            return p
        else if (p.length === props.rowNumber)
            return [...p.slice(0, p.length-1), {word: props.answer, isValid: true}]
        else
            return [...p, {word: props.answer, isValid: true}]
    }, [rawPropositions, props.answer, props.answerShown, props.rowNumber])

    const currentGiftLetters = useMemo<boolean[]>(() => {
        let gifts = [...props.gifts]
        props.propositions.filter(x => x.isValid).forEach(p => {
            for (let k = 0; k < props.answer.length; k++)
                if (props.answer.charAt(k) === p.word.charAt(k))
                    gifts[k] = true
        })
        return gifts
    }, [rawPropositions, props.gifts, props.answer])

    const introductionLetter = useMemo(() => currentGiftLetters[0] ? props.answer.charAt(0) : "", [props.answer, currentGiftLetters])

    useEffect(() => {setEditedProposition(introductionLetter)}, [props.propositions, introductionLetter])

    const submitProposition = useCallback(() => {
        if (!editedProposition) return
        props.onNewProposition(editedProposition)
    }, [editedProposition, props.onNewProposition])

    // callback used when a key is pressed (add the key to the last proposition)
    const onKeyDown = useCallback((key: string) => {
        // check if we are in playing mode
        if (status !== GameStatus.Playing) return
        switch(key) {
            case 'Enter':
                // check if the number of letters is correct
                if (props.answer.length === editedProposition.length) submitProposition()
                break
            case 'Backspace':
                // remove the last letter from the edited proposition
                if (editedProposition.length > introductionLetter.length) setEditedProposition(editedProposition.substring(0, editedProposition.length-1))
                break
            default:
                if (key.trim().length === 1) {
                    // it is a letter
                    const letter = key.trim().toUpperCase()
                    if (editedProposition.length < props.answer.length)
                        setEditedProposition(editedProposition + letter)
                }
        }
    }, [status, props.answer, editedProposition, introductionLetter, submitProposition])

    return <div className="game" style={{"--row-number": props.rowNumber, "--col-number": props.answer.length} as CSSProperties}>
        <div style={{display: 'flex', justifyItems: 'center', alignItems: 'center'}}>
            <Grid propositions={propositions} 
                editedProposition={status === GameStatus.Playing ? editedProposition : undefined} 
                gifts={currentGiftLetters} 
                answer={props.answer} 
                rowNumber={props.rowNumber}
                status={status} />
        </div>
        <Keyboard onKeyPressed={onKeyDown} />
    </div>
}