/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { Dictionary, DictionaryManager, DictionaryName } from "../model/dictionary";

import dictionaries from '../config/dictionaries.json'
import React, { useEffect, useState } from "react";
import { determineGiftLetters } from "../utils/gift";
import { GameWrapperState, getGameWrapperStateURL } from "./GameWrapper";
import { useNavigate } from "react-router-dom";
import { GameConfiguration, loadConfiguration, saveConfiguration } from "../model/configuration";
import { removeDiacritics } from "../utils/stringutils";
import { DictionaryLoadingError, DictionaryStatus } from "./Dictionary";

export const useDictionary = (dictionaryName: DictionaryName) => {
    const [dictionary, setDictionary] = useState<'loading'|DictionaryLoadingError|Dictionary>()

    // if the dictionary changes, we must load it and update its status
    useEffect(() => {
        const loadDictionary = async (abortSignal: AbortSignal) => {
            setDictionary('loading')
            try {
                let dictionary = await DictionaryManager.INSTANCE.getDictionary(dictionaryName, abortSignal)
                setDictionary(dictionary)
            } catch (e) {
                setDictionary(new DictionaryLoadingError(e))
            }
        }

        const abortController = new AbortController();
        loadDictionary(abortController.signal);
        return () => {
            try { abortController.abort(); } catch (e) { console.warn('Error while aborting', e); }
        }
    }, [dictionaryName])

    return dictionary
}


export const GameStarter = () => {
    const [configuration, setConfiguration] = useState(loadConfiguration())
    const dictionary = useDictionary(configuration.dictionaryName)
    const [answer, setAnswer] = useState('') // word to guess
    const navigate = useNavigate()

    // if the configuration changes, we may need to pick a new word
    const wordSelection = configuration.randomWordSelection ? configuration.wordLength : configuration.forcedWord
    useEffect(() => { 
        const updatePickedWord = async () => {
            let word = ''
            if (configuration.randomWordSelection) {
                try {
                    if (dictionary instanceof Dictionary)
                        word = (await dictionary.pickRandomWord(configuration.wordLength)) || ''
                } catch (e) {
                    console.error("Error while picking a word", e)
                }
            } else
                word = configuration.forcedWord
            return word
        }
        updatePickedWord().then(word => setAnswer(word)) 
    }, [dictionary, wordSelection])

    const startGame = () => {
        if (answer) {
            saveConfiguration(configuration)
            const gifts = determineGiftLetters(configuration.giftNumber, answer.length)
            let params: GameWrapperState = {
                dictionaryName: configuration.checkPropositions ? configuration.dictionaryName : undefined, 
                answer: answer, 
                gifts: gifts,
                propositions: [],
                answerShown: false, 
                invalidPropositionsShown: false,
                rowNumber: configuration.rowNumber}
            navigate(getGameWrapperStateURL(params))
        }
    }

    return <div style={{display: 'grid', gridTemplateColumns: 'auto auto', columnGap: '10px', rowGap: '10px', border: 'solid 1px', padding: '10px'}}>
        <div>Dictionary</div>
        <div>
            {Object.entries(dictionaries).map(([name, d]) => {
                return <div key={name}><label>
                    <input type="radio" checked={name === configuration.dictionaryName}
                        onClick={() => setConfiguration(c => { return {...c, dictionaryName: name as DictionaryName}})} />
                    <span style={{textDecoration: name === configuration.dictionaryName && dictionary instanceof DictionaryLoadingError ? 'line-through' : 'none'}}>
                        {`${d.description} `}
                    </span>
                    {name === configuration.dictionaryName && dictionary && <DictionaryStatus dictionary={dictionary} />}
                </label></div>
            })}
            <label>
                <input type="checkbox" checked={configuration.checkPropositions}
                        onChange={(event) => setConfiguration(c => { return {...c, checkPropositions: !c.checkPropositions }})} />
                Check propositions in the dictionary
            </label>
        </div>
        
        <div>Row number</div>
        <div>
            <input size={2} type="number" value={configuration.rowNumber} 
                onChange={event => setConfiguration(c => { return {...c, rowNumber: Math.max(1, event.target.valueAsNumber)} })}/>
        </div>

        <div>Word selection</div>
        <div>
            <div><label>
                    <input type="radio" checked={configuration.randomWordSelection}
                        onClick={() => setConfiguration(c => { return {...c, randomWordSelection: true}})} />
                    <span>Randomly selected word of </span>
                    <input size={2} type="number" value={configuration.wordLength} 
                        onChange={(event) => setConfiguration(c => { return {...c, wordLength: Math.max(2, event.target.valueAsNumber)}})} />
                    <span> letters</span>
            </label></div>
            <div><label>
                <input type="radio" checked={!configuration.randomWordSelection}
                        onClick={() => setConfiguration(c => { return {...c, randomWordSelection: false}})} />
                Forced word: 
                <input type="text" value={configuration.forcedWord} 
                    onChange={(event) => setConfiguration(c => { return {...c, randomWordSelection: false, forcedWord: 
                        removeDiacritics(event.target.value.trim().toUpperCase())}})} />
            </label></div>
        </div>

        <div>Gift letters</div>
        <div>
            <input size={2} type="number" value={configuration.giftNumber} 
                onChange={event => setConfiguration(c => { return {...c, giftNumber: Math.max(0, event.target.valueAsNumber)} })}/>
        </div>
        
        <div style={{gridColumnStart: 1, gridColumnEnd: 'span 2'}}><button style={{width: '100%'}} onClick={() => startGame()} disabled={answer.length === 0}>Start game</button></div>
    </div>
}