/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { Dictionary, DictionaryName } from '../model/dictionary'
import { Link, useLocation, useParams } from 'react-router-dom'

import defaultConfig from '../config/defaultConfig.json'
import dictionaries from '../config/dictionaries.json'
import worday from '../config/worday.json'

import React, { useEffect, useMemo, useState } from 'react'
import { DictionaryStatus } from './Dictionary'
import { WordayData, WordayHistory, computeHash } from '../model/worday'
import { compareYearMonthDay, formatDuration, getCurrentDate, useCurrentDay, YearMonthDay, yearMonthDayToStr } from '../utils/date'
import { determineGiftLetters } from '../utils/gift'
import { Proposition } from './Grid'
import { Game, getGameFeedback } from './Game'
import { GameWrapperState, getGameWrapperStateURL } from './GameWrapper'
import { useDictionary } from './GameStarter'
import { sleep } from '../utils/async'

function obtainGameWrapperState(dictionaryName: DictionaryName, answer: string, propositions: Proposition[]): GameWrapperState {
    let info = ((worday as any)[`${answer.length}`])
    const rowNumber = info.rows || defaultConfig.rowNumber
    const giftNumber = info.gifts || defaultConfig.giftNumber
    const randomizer = () => {
        let v = computeHash(answer) * 31
        return () => {
            let result = v / (1 << 32)
            v *= 31
            return result
        }
    }
    const gifts = determineGiftLetters(giftNumber, answer.length, randomizer())
    return {dictionaryName: dictionaryName, answer: answer, propositions: propositions, rowNumber: rowNumber, answerShown: false, gifts: gifts, invalidPropositionsShown: false}
}

const WordayHistoryElement = (props: {data: WordayData, currentDate: YearMonthDay}) => {
    let today = compareYearMonthDay(props.data.date, props.currentDate) === 0
    let url = today ? 
        `/worday/today/${props.data.dictionaryName}/${props.data.word.length}` :
        getGameWrapperStateURL(obtainGameWrapperState(props.data.dictionaryName, props.data.word, props.data.propositions))
    let foundIndex = props.data.propositions.findIndex(x => x.word === props.data.word)
    if (foundIndex >= 0) return <Link to={url}>{today && '⌛'}✓ {foundIndex+1}</Link>
    else return <Link to={url}>{today && '⌛'}✗</Link>
}

/** Display the stored history */
const WordayHistoryPuzzles = (props: {currentDate: YearMonthDay}) => {
    const data = useMemo(() => WordayHistory.getInstance().getMonthlyDataWithStats(), [])
    return <div style={{display: 'grid'}}>
        {data.monthlyData.filter(x => x.length > 0).map(md => <React.Fragment>
            <div style={{gridColumn: 1, gridRow: `span ${md.length}`}}>{`${md[0].date.year}/${md[0].date.month}`}</div>
            {md.map(d => <React.Fragment>
                <div style={{gridColumn: 2}}>🗓 {d.date.day}</div>
                <div style={{gridColumn: 3}}>{`${dictionaries[d.dictionaryName].description} #${d.word.length}`}</div>
                <div style={{gridColumn: 4}}><WordayHistoryElement data={d} currentDate={props.currentDate} /></div>
            </React.Fragment>)}
        </React.Fragment>)}
    </div>
}

export const WordayGame = () => {
    const currentDay = useCurrentDay()
    const params = useParams()
    
    // manage the dictionary
    const dictionaryName = (params.dictionaryName && params.dictionaryName in dictionaries) ? params.dictionaryName as DictionaryName: 
        defaultConfig.dictionaryName as DictionaryName
    const dictionary = useDictionary(dictionaryName)

    const letterNumber = (params.letterNumber && params.letterNumber in worday) ? parseInt(params.letterNumber) : -1

    const storedWordayData = useMemo(() => WordayHistory.getInstance().getWordayDataElement(currentDay, dictionaryName, letterNumber), 
        // eslint-disable-next-line
        [currentDay.year, currentDay.month, currentDay.day, dictionaryName, letterNumber])

    const answer = useMemo(() => {
        if (storedWordayData)
            return storedWordayData.word
        else if (dictionary instanceof Dictionary && letterNumber > 0) {
            let hashValue = computeHash(`${dictionaryName}/${letterNumber}/${yearMonthDayToStr(currentDay)}`)
            // console.log("Dictionary", dictionary, hashValue, `${dictionaryName}/${letterNumber}/${yearMonthDayToStr(currentDate)}`)
            let word = dictionary.pickWord(letterNumber, hashValue) || ''
            return word
        } else return ''
        // eslint-disable-next-line
    }, [dictionary, currentDay.year, currentDay.month, currentDay.day, letterNumber])

    const [propositions, setPropositions] = useState<Proposition[]>([])

    useEffect(() => {
        if (storedWordayData !== null)
            setPropositions(storedWordayData.propositions)
        else
            setPropositions([])
        // eslint-disable-next-line
    }, [answer, currentDay.year, currentDay.month, currentDay.day])

    useEffect(() => {
        if (answer.length > 0)
            WordayHistory.getInstance().updateWordayDataElement({date: currentDay, dictionaryName: dictionaryName, word: answer, propositions: propositions})
        // eslint-disable-next-line
    }, [currentDay.year, currentDay.month, currentDay.day, dictionaryName, answer, propositions])

    const onNewProposition = useMemo(() => async (word: string) => {
        const isValid = (dictionary as Dictionary).checkWord(word)
        setPropositions(propositions => [...propositions, {word: word, isValid: isValid}])
    }, [dictionary])

    const gameWrapperState = useMemo(() => answer.length > 0 ? obtainGameWrapperState(dictionaryName, answer, propositions) : undefined, 
        [dictionaryName, answer, propositions])

    const gameFeedback = useMemo(() => {
        if (gameWrapperState !== undefined) {
            let feedback = getGameFeedback(gameWrapperState)
            let date = new Date(currentDay.year, currentDay.month-1, currentDay.day).toLocaleDateString()
            let dictionary = `${gameWrapperState.dictionaryName}#${gameWrapperState.answer.length}`
            return `${dictionary}\n${date}\n${document.location.href}\n${feedback}`
        } else return ''
    }, [gameWrapperState])

    const [feedbackCopied, setFeedbackCopied] = useState(false)

    const copyFeedbackInClipboard = async () => {
        await navigator.clipboard.writeText(gameFeedback)
        setFeedbackCopied(true)
        await sleep(2000)
        setFeedbackCopied(false)
    }

    return <React.Fragment>
        {gameWrapperState && <div style={{display: 'flex', flexDirection: 'column', gap: '10px', alignItems: 'center', justifyItems: 'center'}}>
            <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
                <div>{new Date(currentDay.year, currentDay.month-1, currentDay.day).toLocaleDateString()}</div>
                <div style={{width: '5ch'}}> </div>
                <div>⌛{formatDuration(currentDay.remaining)}</div>
            </div>
            <Game {...gameWrapperState} onNewProposition={onNewProposition} />
            <div style={{border: "solid 1px"}} onClick={copyFeedbackInClipboard}>Copy game feedback in the clipboard {feedbackCopied?': copied!':''}</div>
        </div>}
    </React.Fragment>
}

/** Worday puzzles for a given dictionary */
const WordayTodayPuzzlesForDictionary = (props: {dictionary: Dictionary}) => {
    return <ul>
        {Object.entries(worday).map(([k, v]) => 
            <li key={k}><Link to={`/worday/today/${props.dictionary.name}/${k}`}>{parseInt(k)} letters</Link></li>
        )}
    </ul>
}

/** Display the available dictionaries */
const WordayToddayPuzzles = () => {
    const params = useParams()
    const dictionaryName = params.dictionaryName && params.dictionaryName in dictionaries ? params.dictionaryName as DictionaryName : defaultConfig.dictionaryName as DictionaryName
    const dictionary = useDictionary(dictionaryName)
    return <ul>
        {Object.entries(dictionaries).map(([k, v]) => <React.Fragment key={k}>
            {(k === dictionaryName) &&
                <li>
                    <span>{v.description}</span>
                    {dictionary && <span><DictionaryStatus dictionary={dictionary} /></span>}
                    {dictionary instanceof Dictionary && <WordayTodayPuzzlesForDictionary dictionary={dictionary} />}
                </li>}
            {(k !== dictionaryName) &&
                <li>
                    <Link to={`/worday/today/${k}`}>{v.description}</Link>
                </li>}
        </React.Fragment>)}
    </ul>
}

export const WordayUI = () => {
    const currentDate = getCurrentDate()
    return <div style={{border: 'solid 1px', padding: '10px'}}>
        <h2 style={{textAlign: 'center'}}>Worday!</h2>
        <div>
            <WordayToddayPuzzles />
        </div>
        <div>
            <WordayHistoryPuzzles currentDate={currentDate} />
        </div>
    </div>
}
