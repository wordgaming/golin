/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React, { useContext, useEffect, useMemo, useState } from "react"
import { SoundGenerator } from "../model/audio"
import { LetterStatus } from "./Letter"
import { RawWord } from "./RawWord"
import { determinePlacement, ResultMode, Word, WordProps } from "./Word"

import effects from '../config/effects.json'
import { sleep } from "../utils/async"
import { EffectsContext } from "../model/effects"

export const WordWithPossibleEffects = React.memo((props: WordProps & {enableEffects: boolean}) => {
    const effectsContext = useContext(EffectsContext)
    // should not have effectsContext as a dependency
    // eslint-disable-next-line
    const effects = useMemo(() => (effectsContext > 0 && props.enableEffects)? effectsContext: 0, [props.enableEffects])
    if (effects)
        switch (props.resultMode) {
            case ResultMode.None:
            case ResultMode.Editing:
            case ResultMode.InvalidWord:
                // no need of effect
                return <Word {...props} />
            default:
                return <WordWithEffects {...props} effects={effects} />
        }
    else
        return <Word {...props} />
})

const WordWithEffects = React.memo((props: WordProps & {effects: number}) => {
    const [stage, setStage] = useState(0)
    const placement = React.useMemo(() => determinePlacement(props), [props])
    const effecter = async (abortSignal: AbortSignal) => {
        let _stage = 0
        while (_stage <= placement.length) {
            _stage++
            setStage(_stage)
            await sleep(effects.placementStageDelay, abortSignal)
        }
    }
    const s = Math.min(stage, placement.length)
    const stagePlacement = [...placement.slice(0, s), ...Array<LetterStatus>(placement.length-s).fill(LetterStatus.None)]
    
    // Start the effecter with scheduled stages for the placement
    useEffect(() => {
        let abortController = new AbortController();
        effecter(abortController.signal)
        return () => { abortController.abort() }
        // eslint-disable-next-line
    }, [props])

    // Sound effect for the stage
    useEffect(() => {
        let melody: number[] | null = null
        let tempo: number = 0
        if (stage > 0 && stage <= placement.length) {
            let letterStatus = placement[stage-1]
            switch(letterStatus) {
                case LetterStatus.Absent: melody = effects.melodies.absentLetter; break;
                case LetterStatus.Misplaced: melody = effects.melodies.misplacedLetter; break;
                case LetterStatus.Placed: melody = effects.melodies.placedLetter; break;
            }
            tempo = effects.placementStageSoundDuration
        } else if (stage > 0 && stage === placement.length + 1) {
            switch(props.resultMode) {
                case ResultMode.Defeat: melody = effects.melodies.defeat; break;
                case ResultMode.Victory: melody = effects.melodies.victory; break;
                case ResultMode.RevealedAnswer: melody = effects.melodies.revealedAnswer; break;
            }
            tempo = 200
        }
        if (melody) {
            let abortController = new AbortController()
            SoundGenerator.getInstance()?.playMelody(melody, tempo, 1, props.effects, abortController.signal)
            return () => { abortController.abort() }
        }
        // eslint-disable-next-line
    }, [stage])
    
    return <RawWord proposition={props.proposition} status={stagePlacement} invalidWord={false} />
})