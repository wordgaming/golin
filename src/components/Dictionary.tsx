/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { Dictionary } from "../model/dictionary"

export class DictionaryLoadingError {
    constructor(public error: any) {}
}

export const DictionaryStatus = (props: {dictionary: 'loading'|DictionaryLoadingError|Dictionary}) => {
    return <span style={{border: 'solid 1px', marginLeft: '10px'}}>
        {props.dictionary instanceof DictionaryLoadingError && <span title={`${props.dictionary.error}`}>Error while loading</span>}
        {props.dictionary === 'loading' && <span>Loading...</span>}
        {props.dictionary instanceof Dictionary && <span>{`Loaded (${props.dictionary.size.toLocaleString()} words)`}</span>}
    </span>
}
