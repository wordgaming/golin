/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React, { useEffect, useLayoutEffect, useRef, useState } from "react"

import keyboardLayouts from '../config/keyboardLayouts.json'
import defaultConfig from '../config/defaultConfig.json'

import './Keyboard.css'

type KeyboardLayoutName = keyof typeof keyboardLayouts

/** Core of the keyboard that uses a grid */
const KeyboardCore = (props: {layout: string, onKeyPressed: (character: string) => void}) => {
    return <div className="keyboard-core">
        {props.layout.split('\n').map((line, lineIndex) => <React.Fragment key={lineIndex}>
            {line.split('').map((character, characterIndex) => {return character !== ' ' && <div className="keyboard-button" key={character} style={{gridRow: lineIndex+1, gridColumn: characterIndex+1}} 
                onClick={() => props.onKeyPressed(character) }>
                {character.toUpperCase()}
            </div>})}
        </React.Fragment>)}
    </div>
}

/** Layout selector */
const KeyboardLayoutSelector = (props: {selectedLayoutName: string, onLayoutChange: (layoutName: KeyboardLayoutName) => void}) => {
    return <select className="keyboard-chooser" value={props.selectedLayoutName} onChange={event => props.onLayoutChange(event.target.value as KeyboardLayoutName)}>
        {Object.keys(keyboardLayouts).map(k => <option key={k} value={k}>{k}</option> )}
    </select>
}

/** Complete keyboard with layout selection */
export const Keyboard = (props: {onKeyPressed: (character: string, real: boolean) => void}) => {
    const [layoutName, setLayoutName] = useState(defaultConfig.layoutName as KeyboardLayoutName)

    // save the layoutName to localStorage
    useEffect(() => {
        try { localStorage.setItem('layoutName', layoutName)} catch (e) { console.error("Cannot use localStorage to save layoutName")}
    }, [layoutName])

    // save the layout name to localStorage
    useEffect(() => {
        let loadedLayout: string|null = null
        try { 
            loadedLayout = localStorage.getItem('layoutName')
        } catch (e) {}
        if (!loadedLayout || !(loadedLayout in keyboardLayouts)) loadedLayout = defaultConfig.layoutName
        setLayoutName(loadedLayout as KeyboardLayoutName)
    }, [])

    const componentRef = useRef<HTMLDivElement | null>(null)
    // give the focus to the element automatically
    useEffect(() => {componentRef.current?.focus()} ,[])

    return <div ref={componentRef} className="keyboard" onKeyDown={event => props.onKeyPressed(event.key, true)} tabIndex={0} onBlur={(event) => event.target.focus()}>
        <KeyboardLayoutSelector selectedLayoutName={layoutName} onLayoutChange={setLayoutName} />
        <KeyboardCore layout={keyboardLayouts[layoutName]} onKeyPressed={k => props.onKeyPressed(k, false)} />
        <div className="keyboard-button keyboard-enter" onClick={() => props.onKeyPressed('Enter', false)}>⏎</div>
        <div className="keyboard-button keyboard-backspace" onClick={() => props.onKeyPressed('Backspace', false)}>←</div>
    </div>

}