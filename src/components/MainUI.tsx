/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { ResultMode, Word } from "./Word"
import { Outlet, useNavigate } from "react-router-dom"
import { changeEffectsValue, EffectsContext, getEffectsText } from "../model/effects"
import { useState } from "react"


export const MainUI = () => {
    const navigate = useNavigate()
    const [effects, setEffects] = useState(0)

    return <EffectsContext.Provider value={effects}>
        <div style={{display: 'flex', flexDirection: 'column', height: '100vh', width: '100vw'}}>
            <div style={{display: "flex", flexDirection: "row", marginBottom: "20px", justifyContent: "center", height: '50px'}}>
                <button style={{fontSize: "2em"}} onClick={() => setEffects(changeEffectsValue)}>{getEffectsText(effects)}</button>
                <div style={{display: 'flex', alignItems: 'stretch', justifyItems: 'center', aspectRatio: '5/1'}} onClick={() => navigate("/")}><Word proposition="GOLIN" answer="LINGO" resultMode={ResultMode.ValidWord} cursorPosition={-1} /></div>
                <button onClick={() => navigate("/game")}>New game</button>
                <button onClick={() => navigate("/worday")}>Worday!</button>
            </div>
            <div style={{flex: 1, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <Outlet />
            </div>
        </div>
    </EffectsContext.Provider>
}