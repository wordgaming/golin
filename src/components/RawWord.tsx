/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React from "react"
import { Letter, LetterStatus } from "./Letter"

export interface RawWordProps {
    proposition: string // the word that is proposed by the player
    status: LetterStatus[] // the status of each letter
    invalidWord: boolean // if the word is invalid (not in the dictionary), placed and misplaced hints will not be displayed
}

/** Component displaying a word on the grid */
export const RawWord = React.memo((props: RawWordProps) => {
    return <React.Fragment>
        {props.proposition.split("").map((l, index) => <Letter key={index} letter={l} status={props.invalidWord ? LetterStatus.Invalid : props.status[index]} />)}
    </React.Fragment>
})