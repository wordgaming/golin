/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React, { useMemo } from "react";
import { ResultMode, Word } from "./Word";

import './Grid.css'
import { WordWithPossibleEffects } from "./WordWithEffects";
import { GameStatus } from "./Game";

export interface Proposition {
    word: string
    isValid: boolean
}

export interface GridProps {
    rowNumber: number
    answer: string
    propositions: Proposition[] // these propositions must be in result mode
    editedProposition: string|undefined // this proposition is currently edited
    gifts: boolean[] // the position of letters that are given
    status: GameStatus
}

export const Grid = React.memo((props: GridProps) => {
    let edited = props.editedProposition !== undefined ? 1 : 0
    let cursorPosition = props.editedProposition !== undefined ? props.editedProposition.length : -1

    const editedPropositionWithGifts = useMemo(() => {
        if (props.editedProposition === undefined) return undefined
        let v = ""
        for (let k = 0; k < props.answer.length; k++) {
            if (k < props.editedProposition.length)
                v += props.editedProposition.charAt(k)
            else if (props.gifts[k])
                v += props.answer.charAt(k)
            else
                v += '.'
        }
        return v
    }, [props.editedProposition, props.answer, props.gifts])

    const resultMode = useMemo(() => {
        if (props.propositions.length >= 1) {
            let lastProposition = props.propositions[props.propositions.length - 1]
            switch (props.status) {
                case GameStatus.Playing: return (lastProposition.isValid) ? ResultMode.ValidWord : ResultMode.InvalidWord
                case GameStatus.AnswerShown: return ResultMode.RevealedAnswer
                case GameStatus.Defeat: return ResultMode.Defeat
                case GameStatus.Victory: return ResultMode.Victory
            }
        }
        return ResultMode.None
    }, [props.propositions, props.status])

    return <div className="word-grid" style={{gridTemplateColumns: `repeat(${props.answer.length}, 2ch)`}}>
        {props.propositions.map((proposition, index) => {
            // display the first propositions
            return <WordWithPossibleEffects key={index} 
                enableEffects={index === props.propositions.length-1} 
                proposition={proposition.word} 
                answer={props.answer} 
                cursorPosition={-1} 
                resultMode={(index === props.propositions.length-1) ? resultMode : (proposition.isValid ? ResultMode.ValidWord : ResultMode.InvalidWord)} />
        })}
        {editedPropositionWithGifts !== undefined && props.propositions.length + edited <= props.rowNumber &&
            // display the propsition that is currently edited
            <Word proposition={editedPropositionWithGifts} answer={props.answer} cursorPosition={cursorPosition} resultMode={ResultMode.None}  />
        }
        {props.propositions.length + edited < props.rowNumber && Array<number>(props.rowNumber - props.propositions.length - edited).fill(0).map((_, index) => {
            // display blank words for the rows that are not filled
            return <Word key={index} proposition={''.padStart(props.answer.length, ' ')} answer={props.answer} cursorPosition={-1} resultMode={ResultMode.None} />
        })}
    </div>
})