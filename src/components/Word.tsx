/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React from "react";
import { computePlacement } from "../utils/placement";
import { LetterStatus } from "./Letter";
import { RawWord } from "./RawWord";

export enum ResultMode { None, Editing, ValidWord, InvalidWord, Victory, Defeat, RevealedAnswer }

export interface WordProps {
    proposition: string
    cursorPosition: number
    answer: string
    resultMode: ResultMode
}

export function determinePlacement(props: WordProps): LetterStatus[] {
    switch(props.resultMode) {
        case ResultMode.ValidWord:
        case ResultMode.Victory:
        case ResultMode.Defeat:
        case ResultMode.RevealedAnswer:
            return computePlacement(props.proposition, props.answer)
        default:
            let res = Array(props.proposition.length).fill(LetterStatus.None)
            if (props.cursorPosition >= 0) res[props.cursorPosition] = LetterStatus.Typed
            return res
    }
}


export const Word = React.memo((props: WordProps) => {
    const placement = React.useMemo(() => determinePlacement(props), [props])
    return <RawWord proposition={props.proposition} status={placement} invalidWord={props.resultMode === ResultMode.InvalidWord} />
})
