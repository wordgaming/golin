/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React from "react"

import './Letter.css';

export const enum LetterStatus { None = 'none', Typed = 'typed', Absent = 'absent', Placed = 'placed', Misplaced = 'misplaced', Invalid = 'invalid' }

export interface LetterProps {
    letter: string // the letter to display
    status: LetterStatus
}

export const Letter = React.memo((props: LetterProps) => {
    return <div className={`letter letter-${props.status}`}>
        <div>{ props.letter }</div>
    </div>
})