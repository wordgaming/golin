/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { Link } from "react-router-dom"
import { Letter, LetterStatus } from "./Letter"

export const Home = () => {
    return <div>
        <div>
            <h2>Be ready for the Golin game!</h2>
            This is a version of the Golin word game (inspired from <a href="https://en.wikipedia.org/wiki/Lingo_(American_game_show)">Lingo</a> itself inspired from Mastermind). <br />
            Beware, it has not been thoroughly tested, bugs are expected!
        </div>
        <div>
            <h2>How to play?</h2>
            <p>
                Two play modes are available:
                <ol>
                    <li><em>Free game sessions</em>: use the <Link to="/game"><em>New game</em></Link> button to parameterize a new game session with a random mysterious word (or a chosen one to challenge your friends)</li>
                    <li><Link to="/worday"><em>Worday</em></Link>: a new word to guess is proposed every day for each dictionary and number of letters</li>
                </ol>
            </p>
            <p>
                Use your keyboard or the buttons to type a word and validate it with enter. If the word is in the dictionary you will get a feedback about the well-placed and mis-placed letters:
                <ul>
                    <li>A well-placed letter is specified like this: <div style={{display: "inline-flex"}}><Letter letter="A" status={LetterStatus.Placed} /></div></li>
                    <li>If the letter is in the word but not at the same place, it will be indicated like this:  <div style={{display: "inline-flex"}}><Letter letter="A" status={LetterStatus.Misplaced} /></div></li>
                </ul>
            </p>
            <p>Let's play now! Start <Link to="/game">a new game</Link> or try to solve a <Link to="/worday">Worday challenge</Link>.</p>
        </div>
        <div>
            © 2022, <a href="mailto:codefish@online.fr">Codefish</a> - <a href="https://gitlab.com/wordgaming/golin">Source code repository</a> - This software is under <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">the Affero General Public License</a>.
        </div>
    </div>
}