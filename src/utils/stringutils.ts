/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

// Copied from https://developer.mozilla.org/en-US/docs/Glossary/Base64#solution_1_%E2%80%93_escaping_the_string_before_encoding_it
export function utf8_to_b64( str: string ) {
    return window.btoa(unescape(encodeURIComponent( str ))).replaceAll('+', '-').replaceAll('/', '_').replaceAll('=', '');
}

export function b64_to_utf8( str: string ) {
    return decodeURIComponent(escape(window.atob( str.replaceAll('-', '+').replaceAll('_', '/') )));
}


export function removeDiacritics(str: string) {
    return str.normalize("NFKD").replace(/[\u0300-\u036f]/g, "")
}

export function hasNotLetter(str: string) {
    return str.search(/[^A-Za-z]/g) >= 0
}

export function hashString(str: string): number {
    let h = 0
    for(let i = 0; i < str.length; i++)
        h = Math.imul(31, h) + str.charCodeAt(i) | 0
    if (h > 0) return h
    else return h + 2**32
}