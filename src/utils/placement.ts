/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { LetterStatus } from "../components/Letter"

/** Compute whre the letters are well-placed and misplaced */
export function computePlacement(proposition: string, answer: string): LetterStatus[] {
    const result: LetterStatus[] = []
    // initially we don't know the status of the letters
    for (let i = 0; i < proposition.length; i++) result.push(LetterStatus.None)
    
    // first we find the well placed letters
    const len = Math.min(proposition.length, answer.length)
    const answerLetterBag = new Map<string, number>()
    for (let i = 0; i < len; i++)
        if (proposition[i] === answer[i])
            result[i] = LetterStatus.Placed
        else
            answerLetterBag.set(answer[i], (answerLetterBag.get(answer[i]) || 0) + 1) // the letter may be misplaced

    // test for each letter of the proposition if the letter is in the bag of letters candidates for misplacement
    for (let i = 0; i < proposition.length; i++)
        if (result[i] !== LetterStatus.Placed) {
            let n = answerLetterBag.get(proposition[i]) || 0
            if (n === 0)
                result[i] = LetterStatus.Absent // if the letter is not in the bag, it is absent
            else {
                result[i] = LetterStatus.Misplaced
                answerLetterBag.set(proposition[i], n-1) // the letter is in the bag, we decrement the number of samples
            }
        }
    // console.log("Placement", proposition, answer, result)
    return result
}