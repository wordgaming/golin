/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

export function sleep(ms: number, abortSignal: AbortSignal|undefined = undefined) {
    return new Promise((resolve, reject) => {
        if (abortSignal) {
            if (abortSignal.aborted) {
                reject()
                return
            }
            abortSignal.onabort = reject
        }
        setTimeout(resolve, ms)
    });
}
