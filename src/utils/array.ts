/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

export function binarySearch<T, U>(array: T[], keyGetter: (element: T) => U, needle: U, start: number = 0, end: number = -1): number {
    if (end === -1) end = array.length -1
    const middle = Math.floor((start + end) / 2)
    const kmiddle = keyGetter(array[middle])
    if (start === end) {
        if (needle > kmiddle) return -(start+1)-1
        else if (needle < kmiddle) return -start-1
        else return start
    }
    else if (needle > kmiddle) return binarySearch(array, keyGetter, needle, middle+1, end)
    else return binarySearch(array, keyGetter, needle, start, middle)
}

export function keyComparator<T, U>(keyGetter: (element: T) => U): (a: T, b: T) => number {
    return (a: T, b: T) => {
        let ak = keyGetter(a)
        let bk = keyGetter(b)
        if (ak < bk) return -1
        if (ak > bk) return 1
        return 0
    }
}

export function shuffleArray(array: number[]) {
    let output = array.slice()
    for (let i = 0; i < output.length-1; i++) {
        let j = Math.floor(Math.random() * (output.length - i)) + i
        let tmp = output[i]
        output[i] = output[j]
        output[j] = tmp
    }
    return output
}