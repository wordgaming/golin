/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

export function determineGiftLetters(giftNumber: number, wordLength: number, random: () => number = Math.random) {
    giftNumber = Math.min(giftNumber, wordLength-1) // we must not discover all the letters
    // determine the position of the letters that are gift
    let gift = Array(wordLength).fill(false)
    for (let k = 0; k < giftNumber; k++) {
        if (k === 0) gift[0] = true // the initial is revealed
        else {
            let selected = 0
            while (gift[selected]) selected = Math.floor(random() * gift.length)
            gift[selected] = true
        }
    }
    return gift
}