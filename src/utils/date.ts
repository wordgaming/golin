/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { useEffect, useState } from "react"

export interface YearMonthDay {
    year: number
    month: number
    day: number
}

export interface CurrentDay extends YearMonthDay {
    remaining: number
}

export function strToYearMonthDay(str: string) {
    let elements = str.split('-').map(x => parseInt(x))
    if (elements.length === 3 && elements.every(x => isFinite(x)))
        return {year: elements[0], month: elements[1], day: elements[2]}
    else
        return null
}

export function yearMonthDayToStr(ymd: YearMonthDay) {
    return `${ymd.year}-${ymd.month}-${ymd.day}`
}

export function compareYearMonthDay(a: YearMonthDay, b: YearMonthDay): number {
    let cmp = a.year - b.year
    if (cmp !== 0) return cmp
    cmp = a.month - b.month
    if (cmp !== 0) return cmp
    cmp = a.day - b.day
    if (cmp !== 0) return cmp
    return 0
}

export function getCurrentDate(): YearMonthDay {
    let date = new Date()
    return {year: date.getUTCFullYear(), month: date.getUTCMonth() + 1, day: date.getUTCDate()}
}

export function getCurrentDay(): CurrentDay {
    let date = new Date()
    let result = {year: date.getUTCFullYear(), month: date.getUTCMonth() + 1, day: date.getUTCDate(), 
        remaining: (24 - date.getUTCHours()) * 3600 - date.getUTCMinutes() * 60 - date.getUTCSeconds()}
    return result
}

export function useCurrentDay(): CurrentDay {
    const [day, setDay] = useState(getCurrentDay())
    useEffect(() => {
        const handle = setInterval(() => setDay(getCurrentDay()), 1000)
        return () => clearTimeout(handle)
    }, [])
    return day
}

export function formatDuration(duration: number) {
    let hours = Math.floor(duration / 3600)
    let minutes = Math.floor((duration % 3600) / 60)
    let seconds = Math.floor(duration % 60)
    return `${hours}` + ':' + `${minutes}`.padStart(2, '0') + ':' + `${seconds}`.padStart(2, '0')
}