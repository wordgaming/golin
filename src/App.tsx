/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import React from 'react';
import logo from './logo.svg';
import './App.css';
import { MainUI } from './components/MainUI';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { GameWrapper } from './components/GameWrapper';
import { GameStarter } from './components/GameStarter';
import { WordayGame, WordayUI } from './components/Worday';
import { Home } from './components/Home';


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainUI />}>
            <Route index element={<Home />} />
            <Route path="game" element={<GameStarter />} />
            <Route path="game/:answer" element={<GameWrapper />} />
            <Route path="worday" element={<WordayUI />} />
            <Route path="worday/today/:dictionaryName" element={<WordayUI />} />
            <Route path="worday/today/:dictionaryName/:letterNumber" element={<WordayGame />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
