/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { DictionaryName } from "./dictionary"
import dictionaries from '../config/dictionaries.json'
import defaultConfig from '../config/defaultConfig.json'

export interface GameConfiguration {
    dictionaryName: DictionaryName
    checkPropositions: boolean
    randomWordSelection: boolean
    wordLength: number
    forcedWord: string
    rowNumber: number
    giftNumber: number // number of letter that are given (if a>=1, the initial is given in priority)
}

export function loadConfiguration(): GameConfiguration {
    let conf = {...defaultConfig} as GameConfiguration
    try {
        let storedData = localStorage.getItem('configuration')
        let loaded: any = storedData ? JSON.parse(storedData) : {}
        if (loaded.dictionaryName in dictionaries) conf.dictionaryName = loaded.dictionaryName
        if (loaded.checkPropositions !== undefined) conf.checkPropositions = loaded.checkPropositions
        if (loaded.randomWordSelection !== undefined) conf.randomWordSelection = loaded.randomWordSelection
        if (isFinite(parseInt(loaded.wordLength))) conf.wordLength = parseInt(loaded.wordLength)
        if (loaded.forcedWord !== undefined) conf.forcedWord = loaded.forcedWord
        if (isFinite(parseInt(loaded.rowNumber))) conf.rowNumber = loaded.rowNumber
        if (isFinite(parseInt(loaded.giftNumber))) conf.giftNumber = loaded.giftNumber
    } catch (e) {
        console.error("Error while loading configuration from localStorage", e)
    }
    return conf
}

export function saveConfiguration(configuration: GameConfiguration) {
    let toSave: any = {}
    for (let [k, v] of Object.entries(configuration)) {
        if ((defaultConfig as any)[k] !== (configuration as any)[k])
            toSave[k] = v
    }
    try {
        localStorage.setItem('configuration', JSON.stringify(toSave))
    } catch (e) {
        console.error("Cannot save the configuration to localStorage")
    }
}
