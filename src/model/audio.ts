/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { sleep } from "../utils/async";

export class SoundGenerator {
    private audioContext: AudioContext
    private oscillator: OscillatorNode
    private gainNode: GainNode

    constructor() {
        this.audioContext = new window.AudioContext()
        this.oscillator = this.audioContext.createOscillator()
        this.gainNode = this.audioContext.createGain()
        this.gainNode.connect(this.audioContext.destination)
        this.oscillator.connect(this.gainNode);
        this.gainNode.gain.value = 0
        this.oscillator.start()
    }

    playSine(frequency: number, gain: number) {
        this.oscillator.type = 'sine'
        this.oscillator.frequency.value = frequency
        this.gainNode.gain.value = gain
    }

    stop() {
        this.gainNode.gain.value = 0
        this.oscillator.frequency.value = 0
    }

    async playMelody(score: number[], period: number, repeats: number = 1, gain: number = 1, abortSignal: AbortSignal|undefined = undefined) {
        console.debug("Playing melody", score, period)
        for (let i = 0; i < score.length; i += 2) {
            let frequency = score[i]
            let duration = score[i+1] * period
            this.playSine(frequency, gain)
            try {
                await sleep(duration, abortSignal)
            } catch (e) {
                return
            } finally {
                this.stop()
            }
        }
    }

    private static INSTANCE: SoundGenerator| null = null

    public static getInstance(): SoundGenerator|null {
        if (SoundGenerator.INSTANCE === null && 'AudioContext' in window)
            return new SoundGenerator()
        else
            return SoundGenerator.INSTANCE
    }
}