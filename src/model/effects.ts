/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import { createContext } from "react";

export const EffectsContext = createContext(0)

const EFFECTS_VALUES = [
    {value: 0, text: '🔇'},
    {value: 0.5, text: '🔉'},
    {value: 1, text: '🔊'}
]

export function getEffectsText(value: number) {
    return EFFECTS_VALUES.find(x => x.value === value)!.text
}

export function changeEffectsValue(oldValue: number) {
    let i = (EFFECTS_VALUES.findIndex(x => x.value === oldValue) + 1) % EFFECTS_VALUES.length
    return EFFECTS_VALUES[i].value
}