/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import dictionaries from '../config/dictionaries.json'

import { binarySearch } from "../utils/array"

import { sleep } from "../utils/async"
import { hasNotLetter, removeDiacritics } from "../utils/stringutils"

export type DictionaryName = keyof typeof dictionaries

export class Dictionary {
    public readonly size: number
    
    // we assume that the content is already sorted
    constructor(public name: DictionaryName, private content: Map<number, string[]>) {
        let size = 0
        content.forEach(value => size += value.length)
        this.size = size
    }

    private getWordList(wordLength: number): string[] {
        let wordList = this.content.get(wordLength)
        return wordList ? wordList : []
    }

    getWordNumber(wordLength: number): number {
        return this.getWordList(wordLength).length
    }

    pickRandomWord(wordLength: number): string|undefined {
        let wordList = this.getWordList(wordLength)
        if (wordList.length === 0) return undefined
        return wordList[Math.floor(Math.random() * wordList.length)]
    }

    pickWord(wordLength: number, index: number): string|undefined {
        let wordList = this.getWordList(wordLength)
        if (wordList.length === 0) return undefined
        return wordList[index % wordList.length]
    }
    
    // function checking if a word is present in the dictionary
    checkWord(word: string): boolean {
        return binarySearch(this.getWordList(word.length), x => x, word) >= 0
    }
}

export class DictionaryManager {
    private lastDictionary: Dictionary|undefined = undefined

    private async createDictionary(name: DictionaryName, abortSignal: AbortSignal|undefined = undefined) {
        let path = `/dictionaries/${name}.txt`
        let wordListText = await (await fetch(path, {signal: abortSignal})).text()
        let wordList = wordListText.split('\n')
        wordListText = ''
        await sleep(0, abortSignal)
        let wordsByLength = new Map<number, string[]>()
        for (let i = 0; i < wordList.length; i++) {
            if (i % 10000 === 0) await sleep(0, abortSignal)
            let word = removeDiacritics(wordList[i].trim()).toUpperCase()
            if (! hasNotLetter(word)) {
                let list = wordsByLength.get(word.length)
                if (! list) {
                    list = []
                    wordsByLength.set(word.length, list)
                }
                list.push(word)
            }
        }
        await sleep(0, abortSignal)
        
        // we sort each word list for extra caution
        for (let v of Array.from(wordsByLength.values())) {
            v.sort()
            await sleep(0, abortSignal)
        }
        
        return new Dictionary(name, wordsByLength)
    }

    async getDictionary(name: DictionaryName, abortSignal: AbortSignal|undefined = undefined) {
        if (this.lastDictionary?.name === name) return this.lastDictionary
        this.lastDictionary = await this.createDictionary(name, abortSignal)
        return this.lastDictionary
    }

    // singleton instance
    static INSTANCE = new DictionaryManager()
}
