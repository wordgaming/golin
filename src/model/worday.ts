/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

import secrets from '../config/secrets.json'
import dictionaries from '../config/dictionaries.json'

import { b64_to_utf8, hashString, utf8_to_b64 } from '../utils/stringutils'
import { getCurrentDate, YearMonthDay } from '../utils/date'
import { DictionaryName } from './dictionary'
import { binarySearch, keyComparator } from '../utils/array'
import { Proposition } from '../components/Grid'

/** Compute (a non-crypto secure) hash value for a string */
export function computeHash(str: string) {
    let s = str + secrets.wordayHashSecret
    return hashString(s)
}

export interface WordayData {
    dictionaryName: DictionaryName
    word: string
    date: YearMonthDay
    propositions: Proposition[]
}

export function encodeWordayGame(data: WordayData) {
    let obj = {...data}
    obj.word = utf8_to_b64(data.word)
    return obj
}

export function decodeWordayGame(obj: any): WordayData|null {
    let result: any = {}
    result.dictionaryName = obj.dictionaryName in dictionaries ? obj.dictionaryName : undefined
    result.word = obj.word && b64_to_utf8(obj.word)
    result.date = obj.date
    result.propositions = obj.propositions ? obj.propositions : []
    if (!result.dictionaryName || !result.word || !result.date) return null
    return result as WordayData
}

export function wordayDataKeyGetter(a: WordayData): [number, number, number, string, number] {
    return [a.date.year, a.date.month, a.date.day, a.dictionaryName, a.word.length]
}

export function loadWordayHistory(months: number): WordayData[][] {
    if (! ('localStorage' in window)) return []
    const ls = window.localStorage
    // laod the history of the last year
    const currentDate = getCurrentDate()
    let year = currentDate.year
    let month = currentDate.month
    let result: WordayData[][] = []
    for (let i = 0; i <= months; i++) {
        try {
            let rawData = ls.getItem(`wordayHistory/${year}-${month}`) || ''
            if (rawData.length > 0) {
                let monthlyData = (JSON.parse(rawData) as any[]).map(decodeWordayGame).filter(x => x !== null) as WordayData[]
                monthlyData.sort(keyComparator(wordayDataKeyGetter))
                result.push(monthlyData)
            }
        } catch (e) {
            console.error('Error while loading worday data from localStorage')
        }
        month -= 1
        if (month === 0) { month = 12; year -= 1; }
    }
    return result
}

export function saveWordayHistory(year: number, month: number, history: WordayData[]) {
    try {
        localStorage.setItem(`wordayHistory/${year}-${month}`, JSON.stringify(history.map(x => encodeWordayGame(x))))
    } catch (e) {
        console.error("Cannot save the worday history", e)
    }
}

export interface WordayStats {
    successes: number
    failures: number
    distribution: number[]
}

export class WordayHistory {
    private elements: WordayData[][]
    
    constructor() {
        this.elements = loadWordayHistory(12)
    }

    getWordayDataElement(date: YearMonthDay, dictionaryName: DictionaryName, wordLength: number) {
        for (let monthlyData of this.elements)
            if (monthlyData[0] && monthlyData[0].date.year === date.year && monthlyData[0].date.month === date.month) {
                let pos = binarySearch(monthlyData, wordayDataKeyGetter, [date.year, date.month, date.day, dictionaryName, wordLength])
                if (pos >= 0) return monthlyData[pos]
            }
        return null
    }

    updateWordayDataElement(data: WordayData) {
        for (let i = 0; i < this.elements.length; i++) {
            const monthlyData = this.elements[i]
            if (monthlyData[0] && monthlyData[0].date.year === data.date.year && monthlyData[0].date.month === data.date.month) {
                let monthlyData2 = monthlyData
                let pos = binarySearch(monthlyData, wordayDataKeyGetter, wordayDataKeyGetter(data))
                if (pos >= 0) 
                    monthlyData2 = [...monthlyData.slice(0, pos), data, ...monthlyData.slice(pos+1)]
                else if (pos < 0) {
                    pos = - pos - 1
                    monthlyData2 = [...monthlyData.slice(0, pos), data, ...monthlyData.slice(pos)]
                }
                this.elements[i] = monthlyData2
                saveWordayHistory(data.date.year, data.date.month, monthlyData2)
                return
            }
        }
        this.elements = [[data], ...this.elements]
        saveWordayHistory(data.date.year, data.date.month, this.elements[0])
        return
    }

    getMonthlyDataWithStats() {
        let dictionaries = Array.from(new Set(this.elements.flatMap(x => x.map(y => `${y.dictionaryName}/${y.word.length}`))))
        let dictionaries2 = dictionaries.map(x => { let [name, len] = x.split('/'); return [name, parseInt(len)] as [string, number]; } )
        dictionaries2.sort()
        let globalStats: WordayStats[] = Array(dictionaries2.length).fill(0).map(x => { return {successes: 0, failures: 0, distribution: []}})
        let allMonthlyStats: WordayStats[][] = []
        for (let monthData of this.elements) {
            let monthlyStats = Array(dictionaries2.length).fill(0).map(x => { return {successes: 0, failures: 0, distribution: []}})
            for (let element of monthData) {
                let dictionaryIndex = dictionaries2.findIndex(x => x[0] === element.dictionaryName && x[1] === element.word.length)
                if (dictionaryIndex < 0) throw new Error("Cannot find the dictionary")
                for (let stats of [globalStats, monthlyStats]) {
                    const success = element.propositions.findIndex(x => x.word === element.word)
                    stats[dictionaryIndex][(success >= 0) ? 'successes' : 'failures']++
                    if (success >= 0) {
                        let current = stats[dictionaryIndex].distribution[success]
                        stats[dictionaryIndex].distribution[success] = (current ? current : 0) + 1
                    }
                }
            }
            allMonthlyStats.push(monthlyStats)
        }
        return { monthlyData: this.elements, dictionaries: dictionaries2, globalStats: globalStats, monthlyStats: allMonthlyStats }
    }

    private static INSTANCE: WordayHistory

    static getInstance() {
        if (! this.INSTANCE)
            this.INSTANCE = new WordayHistory()
        return this.INSTANCE
    }
}