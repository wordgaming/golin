#! /usr/bin/env node

/**
 * project: Golin
 * author: codefish <codefish@online.fr>
 * license: Affero GPL 3 [https://www.gnu.org/licenses/agpl-3.0.txt]
 */

const fs = require('fs');
const https = require('https');

const download = (url, dest, cb) => {
    var file = fs.createWriteStream(dest + '.tmp');
    var request = https.get(url, function(response) {
      response.pipe(file);
      file.on('finish', function() {
        file.close(() => {
            fs.rename(dest + '.tmp', dest, err => {
                if (err && cb) cb(err)
            })
        })
      });
    }).on('error', function(err) { // Handle errors
      fs.unlink(dest + '.tmp', (err) => {}); // Delete the file async. (But we don't check the result)
      if (cb) cb(err.message);
    });
};

let dictionaries = JSON.parse(fs.readFileSync('./src/config/dictionaries.json'))
Object.entries(dictionaries).forEach(([k, v]) => {
    const path = v.path
    download(v.path, `./public/dictionaries/${k}.txt`, (err) => { console.error('Error while downloading %s at path %s: %s', k, v.path, err)} )
})
